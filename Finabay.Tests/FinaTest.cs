﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Finabay.Controllers;
using Finabay.ViewModels;
using Finabay.Models;

namespace Finabay.Tests
{
    [TestClass]
    public class FinaTest
    {
        public ApplicationDbContext GetDb()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Directory.GetCurrentDirectory());
            return new ApplicationDbContext();
        }

        [TestMethod]
        public void TestIpResolver()
        {
            var result = Finabay.External.CountryResolver.ResolveCountryByIP("85.254.158.128");
            Assert.AreEqual(result, "LV");
        }

        [TestMethod]
        public void TestBlacklist()
        {
            var controller = new BlacklistController(GetDb());
            var person = new BlacklistPerson() { PersonId = "111111-11111" };
            var id = controller.Post(person);
            Assert.IsNotNull(id);

            var blacklist = controller.Get(id.Value);
            Assert.AreEqual(person.PersonId, blacklist.PersonId);

            var loanController = new LoanController(GetDb());
            var loan = new LoanRequest() { Amount = 100, FirstName = "a", LastName = "a", LoanTermMonths = 1, PersonId = "111111-11111" };
            var result = loanController.Post(loan);
            Assert.AreEqual(result.ApplicationStatus, ApplicationStatus.Rejected);
        }

        [TestMethod]
        public void TestListLoan()
        {
            LoanController controller = new LoanController(GetDb());
            controller.Request = new HttpRequestMessage();

            var loansList = controller.Get();
            Assert.IsNotNull(loansList);
        }

        [TestMethod]
        public void TestApplyLoan()
        {
            LoanController controller = new LoanController(GetDb());
            controller.Request = new HttpRequestMessage();
            //controller.RequestContext.

            var loan = new LoanRequest()
            {
                PersonId = "111111-10101",
                FirstName = "Sam",
                LastName = "Pirozhkov",
                Amount = 10000,
                LoanTermMonths = 36,
            };

            var added = controller.Post(loan);
            Assert.AreEqual(added.ApplicationStatus, ApplicationStatus.Accepted);
            var retrieved = controller.Get(added.LoanId.Value);
            Assert.IsNotNull(retrieved);
            Assert.AreNotEqual(retrieved.LoanId, 0);
            Assert.AreEqual(loan.PersonId, retrieved.PersonId);
        }

        [TestMethod]
        public void TestExceedRate()
        {
            LoanController controller = new LoanController(GetDb());
            controller.Request = new HttpRequestMessage();

            var loan1 = new LoanRequest() { Amount = 100, FirstName = "a", LastName = "a", LoanTermMonths = 1, PersonId = "121212-12321" };
            var loan2 = new LoanRequest() { Amount = 100, FirstName = "a", LastName = "a", LoanTermMonths = 1, PersonId = "121212-12322" };
            var loan3 = new LoanRequest() { Amount = 100, FirstName = "a", LastName = "a", LoanTermMonths = 1, PersonId = "121212-12323" };
            var loan4 = new LoanRequest() { Amount = 100, FirstName = "a", LastName = "a", LoanTermMonths = 1, PersonId = "121212-12324" };
            var loan5 = new LoanRequest() { Amount = 100, FirstName = "a", LastName = "a", LoanTermMonths = 1, PersonId = "121212-12325" };
            var res1 = controller.Post(loan1);
            var res2 = controller.Post(loan2);
            var res3 = controller.Post(loan3);
            var res4 = controller.Post(loan4);
            var res5 = controller.Post(loan5);

            Assert.AreEqual(res1.ApplicationStatus, ApplicationStatus.Accepted);
            Assert.AreEqual(res5.ApplicationStatus, ApplicationStatus.Rejected);
        }

        [TestMethod]
        public void TestReject()
        {
            LoanController controller = new LoanController(GetDb());
            //controller.Request = new HttpRequestMessage();

            //controller.Reject(1);
        }
    }
}
