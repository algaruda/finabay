﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using Finabay.Models;
using Finabay.External;

namespace Finabay.Common
{
    public static class LoanLogic
    {
        public static bool CheckInBlacklist(string personId, ApplicationDbContext dbContext)
        {
            return dbContext.Blacklist.Any(o => o.PersonId == personId);
        }

        public static bool CheckExceedRate(Loan loan, ApplicationDbContext dbContext)
        {
            var limitPerSecond = 0;
            if (!int.TryParse(ConfigurationManager.AppSettings["LimitPerSecond"], out limitPerSecond))
                limitPerSecond = 5;


            var dateCheck = loan.ApplicationDate.AddSeconds(-1);

            var result = dbContext.Loans.Count(o => o.CountryCode == loan.CountryCode
                                                && o.ApplicationDate > dateCheck);

            if (result >= limitPerSecond)
                return true;

            return false;
        }
    }
}