﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Profit365.GeoIP;

namespace Finabay.External
{
    public static class CountryResolver
    {
        public static string ResolveCountryByIP(string ip)
        {
            GeoLocationProvider provider = new GeoLocationProvider();
            var data = provider.GetIpDataBy_IP_API_dot_com(ip);

            if (data == null || string.IsNullOrEmpty(data.countryCode))
                return "LV";
            else
                return data.countryCode.ToUpper();
        }
    }
}