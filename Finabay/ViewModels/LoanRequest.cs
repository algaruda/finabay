﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finabay.ViewModels
{
    public class LoanRequest
    {
        public decimal Amount { get; set; }
        public int LoanTermMonths { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonId { get; set; }
    }
}