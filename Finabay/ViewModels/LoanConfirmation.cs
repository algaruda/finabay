﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Finabay.Models;

namespace Finabay.ViewModels
{
    public class LoanConfirmation
    {
        public int LoanId { get; set; }
        public LoanStatus Status { get; set; }
        public string Reason { get; set; }
    }
}