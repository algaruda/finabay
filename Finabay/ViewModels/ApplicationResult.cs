﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finabay.Models
{
    public class ApplicationResponse
    {
        public ApplicationStatus ApplicationStatus { get; set; }
        public string Reason { get; set; }
        public int? LoanId { get; set; }

        public ApplicationResponse(ApplicationStatus status, string reason = null, int? LoanId = null)
        {
            this.ApplicationStatus = status;
            this.Reason = reason;
            this.LoanId = LoanId;
        }
    }

    public enum ApplicationStatus
    {
        Rejected = 0,
        Accepted = 1,
    }
}