﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net.Http;
using System.Data.Entity;
using System.Data.Entity.Validation;
using NLog;
using Finabay.Models;
using Finabay.ViewModels;
using Finabay.Common;
using Microsoft.AspNet.Identity;

namespace Finabay.Controllers
{
    [Authorize]
    public class LoanController : ApiController
    {
        private ApplicationDbContext dbContext { get; set; }
        public Logger log;

        public LoanController()
        {
            this.dbContext = new ApplicationDbContext();
            this.log = LogManager.GetLogger("logfile");
        }

        public LoanController(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
            this.log = LogManager.GetLogger("logfile");
        }

        public IEnumerable<Loan> Get()
        {
            return dbContext.Loans.ToList();
        }

        public Loan Get(int id)
        {
            return dbContext.Loans.FirstOrDefault(o => o.LoanId == id);
        }

        [Route("api/loan/GetByUserId/{id}")]
        public ICollection<Loan> GetApprovedByUserId(string id)
        {
            var items = dbContext.Loans.Where(o => o.ConfirmedBy != null && o.ConfirmedBy.Id == id && o.LoanStatus == LoanStatus.Approved).ToList();
            return items;
        }

        [HttpPost]
        public ApplicationResponse Post(LoanRequest request)
        {
            if (!ModelState.IsValid)
            {
                return new ApplicationResponse(ApplicationStatus.Rejected, "Incorrect loan application request");
            }

            var loan = new Loan()
            {
                ApplicationDate = DateTime.Now,
                Amount = request.Amount,
                FirstName = request.FirstName,
                LastName = request.LastName,
                PersonId = request.PersonId,
                LoanTermMonths = request.LoanTermMonths,
            };

            var id = User.Identity.GetUserId();
            if (id != null)
            {
                var user = dbContext.Users.FirstOrDefault(o => o.Id == id);
                loan.CreatedBy = user;
            }

            try
            {
                if (LoanLogic.CheckInBlacklist(loan.PersonId, dbContext))
                {
                    return new ApplicationResponse(ApplicationStatus.Rejected, "Blacklisted person id");
                }

                var owin = Request.GetOwinContext(); // check if this was called as local test
                if(owin == null)
                {
                    loan.CountryCode = "LV";
                }
                else
                {
                    var ipAddress = owin.Request.RemoteIpAddress;
                    loan.CountryCode = External.CountryResolver.ResolveCountryByIP(ipAddress);
                }

                if (LoanLogic.CheckExceedRate(loan, dbContext))
                {
                    return new ApplicationResponse(ApplicationStatus.Rejected, "Too many requests per second from this country");
                }

                dbContext.Loans.Add(loan);
                dbContext.SaveChanges();
            }
            catch(Exception ex)
            {
                log.Error("Error Loan POST: " + ex.Message + " : " + ex.Data);
                return new ApplicationResponse(ApplicationStatus.Rejected, ex.Message + "Unknown error : " + ex.Data);
            }

            return new ApplicationResponse(ApplicationStatus.Accepted, "Ok", loan.LoanId);
        }

        [HttpPost]
        [Route("api/loan/ConfirmOrReject/")]
        public ApplicationResponse ConfirmOrReject(LoanConfirmation confirmation)
        {
            try
            {
                var item = dbContext.Loans.FirstOrDefault(o => o.LoanId == confirmation.LoanId);
                if(item != null)
                {
                    item.LoanStatus = confirmation.Status;
                    item.Notes = confirmation.Reason;
                    item.ConfirmDate = DateTime.Now;

                    var id = User.Identity.GetUserId();
                    if(id != null)
                    {
                        var user = dbContext.Users.FirstOrDefault(o => o.Id == id);
                        item.ConfirmedBy = user;
                    }
                    else
                    {
                        // @! saved from tests
                        return new ApplicationResponse(ApplicationStatus.Rejected, "User not found");
                    }

                    dbContext.SaveChanges();
                }
                else
                {
                    return new ApplicationResponse(ApplicationStatus.Rejected, "Not found LoanId: " + confirmation.LoanId);
                }
            }
            catch(Exception ex)
            {
                log.Error("Error ConfirmOrReject: " + ex.Message + " : " + ex.Data);
                return new ApplicationResponse(ApplicationStatus.Rejected, ex.Message + "Unknown error : " + ex.Data);
            }

            return new ApplicationResponse(ApplicationStatus.Accepted);
        }
    }


}