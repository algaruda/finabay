﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Finabay.Models
{
    public class Loan
    {
        public int LoanId { get; set; }
        public decimal Amount { get; set; }
        public int LoanTermMonths { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonId { get; set; }
        public LoanStatus LoanStatus { get; set; }
        public string Notes { get; set; }
        public string CountryCode { get; set; }

        public DateTime ApplicationDate { get; set; }
        public virtual ApplicationUser CreatedBy { get; set; }

        public DateTime? ConfirmDate { get; set; }
        public virtual ApplicationUser ConfirmedBy { get; set; }
    }

    public enum LoanStatus
    {
        New = 0,
        Approved = 1,
        Rejected = 2
    }
}