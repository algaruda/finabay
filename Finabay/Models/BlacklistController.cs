﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net.Http;
using System.Data.Entity;
using System.Data.Entity.Validation;
using Finabay.Models;
using Finabay.Common;

namespace Finabay.Models
{
    [Authorize]
    public class BlacklistController : ApiController
    {
        private ApplicationDbContext dbContext { get; set; }

        public BlacklistController()
        {
            this.dbContext = new ApplicationDbContext();
        }

        public BlacklistController(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public IEnumerable<BlacklistPerson> Get()
        {
            return dbContext.Blacklist.ToList();
        }

        public BlacklistPerson Get(int id)
        {
            return dbContext.Blacklist.FirstOrDefault(o => o.Id == id);
        }

        [HttpPost]
        public int? Post(BlacklistPerson blacklistPerson)
        {
            try
            {
                var person = dbContext.Blacklist.FirstOrDefault(o => o.PersonId == blacklistPerson.PersonId);

                if(person != null)
                {
                    return person.Id;
                }
                else
                {
                    dbContext.Blacklist.Add(blacklistPerson);
                    dbContext.SaveChanges();
                }

                return blacklistPerson.Id;
            }
            catch
            {
                return null;
            }
        }
    }
}