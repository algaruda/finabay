﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Validation;
using Finabay.Models;
using System.Diagnostics;

namespace Finabay.DAL
{
    public class UnitOfWork
    {
        private ApplicationDbContext _context = null;
        private GenericRepo<Loan> _loanRepository;
        private GenericRepo<BlacklistPerson> _blacklistRepository;
        //private GenericRepo<ApplicationUser> _userRepository;
        //private GenericRepo<Token> _tokenRepository;

        public UnitOfWork()
        {
            _context = new ApplicationDbContext();
        }

        #region Public Repository Creation properties
        public GenericRepo<Loan> LoanRepo
        {
            get
            {
                if (this._loanRepository == null)
                    this._loanRepository = new GenericRepo<Loan>(_context);
                return _loanRepository;
            }
        }

        public GenericRepo<BlacklistPerson> BlacklistRepo
        {
            get
            {
                if (this._blacklistRepository == null)
                    this._blacklistRepository = new GenericRepo<BlacklistPerson>(_context);
                return _blacklistRepository;
            }
        }
        #endregion

        #region Public member methods
        public void Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch(DbEntityValidationException e)
            {
                var outputLines = new List<string>();
                foreach(var eve in e.EntityValidationErrors)
                {
                    outputLines.Add(string.Format(
                        "{0}: Entity of type \"{1}\" in state \"{2}\" has the following validation errors:", DateTime.Now,
                        eve.Entry.Entity.GetType().Name, eve.Entry.State));

                    foreach(var ve in eve.ValidationErrors)
                    {
                        outputLines.Add(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                    }
                }
                System.IO.File.AppendAllLines("errors.txt", outputLines);

                throw e;
            }

        }
        #endregion

        #region IDisposable
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if(disposing)
                {
                    Debug.WriteLine("ŪnitOfWork is being disposed");
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}